ARG rb_version
ARG os_version

FROM ruby:${rb_version}-slim-${os_version}

RUN apt-get update && \
    apt-get install -y python3-pip \
      python3-restructuredtext-lint \
      shunit2 \
      spell \
      hunspell && \
    rm -rf /var/lib/apt/lists/*

RUN gem install mdl
RUN pip3 install pyspelling==2.4

COPY spell /usr/local/bin/spell
COPY .mdlrc /
COPY .spellrc /
COPY README.md /
COPY runtests.sh /
COPY tests /tests

RUN chmod +x /usr/local/bin/spell /runtests.sh /tests/*.sh
RUN ./runtests.sh;

ENTRYPOINT /bin/bash
