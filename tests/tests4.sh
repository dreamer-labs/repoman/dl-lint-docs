#!/bin/bash

testRSTLint() {

  # Tests missing bin failure condition
  assertSame "WARNING mock/dirtyrst.rst:26 Literal block ends without a blank line; unexpected unindent." "$(rst-lint mock/dirtyrst.rst 2>&1)";
  assertSame "INFO File mock/cleanrst.rst is clean." "$(rst-lint mock/cleanrst.rst 2>&1)";

};

source $(which shunit2);
