#!/bin/bash

oneTimeSetUp() {

  # Export path so that spell wrapper is in path
  export OLDPATH="$PATH"
  export PATH="$(realpath $(dirname ../${0})):$PATH";
  # Remove spell binary to test missing bin failure condition
  mv /usr/bin/spell{,.bak};

};

testSpellMissingCases() {

  # Tests missing bin failure condition
  assertSame "[FAIL] /usr/bin/spell not installed" "$(spell mock/cleanfile.txt 2>&1)";
  mv /usr/bin/spell{.bak,};
  export PATH="$OLDPATH";

};

source $(which shunit2);
