#!/bin/bash

echo "[INFO] Tesing README.md for markdown syntax and style errors, using markdownlint (mdl) and .mdlrc in repo (if present)."
mdl ../README.md;
cd $OLDPWD;
