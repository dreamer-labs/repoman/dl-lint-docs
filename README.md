# dl-lint-docs

Builds docker container image used to lint documentation.

## Purpose

The Docker container built from this repository can be used to perform spell checking and markdown linting off README files and other documentation written in markdown format. It uses `spell`, `mdl`, and `pyspelling` (markdownlint) to do so.

## Usage

### Spell

The GNU `spell` program is a command-line utility that parses a file for words not listed in its default dictionary.

Since, this can lead to quite a few false positives, `spell` provides users with the option of passing in a supplemental word list to further whitelist false positives. This word list is a one-word-per-line plain-text file listing all the strings to whitelist.

Example:

```
:/# echo "This is a sample txt file with some missspelled words." > test.txt
:/# spell test.txt
txt
missspelled
:/# sed -i 's/missspelled//' test.txt
:/# spell test.txt
txt
:/# spell test.txt | sort -u  > .spellrc
:/# spell -d .spellrc test.txt
```

A "gotcha" with `spell` is that it returns code "0" regardless of if it finds errors or not. An external mechanism is needed to catch errors in a non-interactive fashion as a result. For example:

```
spell_output="$(spell anothertest.txt)"
if [[ "${#spell_output}" -gt 0 ]]
then
  echo ${spell_output}
  exit 1
fi
```

The container takes care of this for the user and abstracts it from the user by creating a bash function named `spell` that wraps the actual `spell` binary and makes it behave in a more desirable manner for a non-interactive use case. This wrapper function should be transparent to the user in most cases.

#### mdl (markdownlint)

`mdl` is a tool that parses markdown files for common syntax errors. It also enforces certain readability standards. It can be customized to ignore certain rules by using a file called `.mdlrc` that modifies the behavior of the utility. A common standard that is ignored is character limits per line. Reference the `mdl` documentation for more details: [here](https://github.com/markdownlint/markdownlint/blob/master/docs/configuration.md).

As a practical example, here is how a user would ignore the line length rule:

```
:/# mdl README.md
README.md:65: MD009 Trailing spaces
README.md:67: MD009 Trailing spaces
README.md:7: MD013 Line length
README.md:13: MD013 Line length
README.md:15: MD013 Line length
README.md:31: MD013 Line length
README.md:42: MD013 Line length
README.md:46: MD013 Line length
README.md:62: MD013 Line length
README.md:75: MD013 Line length
README.md:79: MD013 Line length
README.md:130: MD013 Line length
README.md:143: MD013 Line length

A detailed description of the rules is available at https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md
:/# echo 'rules "~MD013"' > .mdlrc
:/# cat .mdlrc
rules "~MD013"
:/# mdl README.md
README.md:65: MD009 Trailing spaces
README.md:67: MD009 Trailing spaces

A detailed description of the rules is available at https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md
```

#### rst-lint

`rst-lint` is a python utility (shipped as a debian package) used to lint `.rst` files. This utility offers no native config/filtering capabilities and is a strict pass or fail.

```
:/# /usr/bin/rst-lint tests/mock/dirtyrst.rst
WARNING tests/mock/dirtyrst.rst:26 Literal block ends without a blank line; unexpected unindent.
:/# /usr/bin/rst-lint tests/mock/cleanrst.rst
INFO File tests/mock/cleanrst.rst is clean.
```

#### pyspelling

https://facelessuser.github.io/pyspelling/

Pyspelling depends on aspell or hunspell... this image installs hunspell because it handles urls, html and other document types a little better then aspell.

Use a .pyspelling.yml config file to define the language dictionary, word lists and filters etc. used for your stage. See: https://facelessuser.github.io/pyspelling/configuration/  


**Config file .pyspelling.yml**
```yaml
matrix:
- name: Spell Checker
  hunspell:
    d: en_US
  dictionary:
    wordlists:
    - ./allowed_words.dic
  sources:
  - source/*.rst
  pipeline:
  - pyspelling.filters.text:
```

**Execute Command**
`pyspelling --spellchecker hunspell`


### Container Image

This container can either be used in a Dockerfile as a base image or pulled down for use by referencing the image URL found here:

`https://gitlab.com/dreamer-labs/dl-lint-docs/container_registry`

Ensure you select the image that matches the desired commit hash the image was built from. This acts as a version tag.

### Examples

The following examples use the sample image name:

`registry.gitlab.com/dreamer-labs/dl-lint-docs:{commit_hash}-lint-docs`

Replace this name with the name  of your desired image.

#### Pulling for Direct Use

```
docker pull registry.gitlab.com/dreamer-labs/dl-lint-docs:{commit_hash}-lint-docs
```

#### Using as a Base Image in Another Dockerfile

```
FROM registry.gitlab.com/dreamer-labs/dl-lint-docs:{commit_hash}-lint-docs
```

#### Using in a .gitlab-ci.yml file for linting docs

```
---
image: registry.gitlab.com/dreamer-labs/dl-lint-docs:{commit_hash}-lint-docs

stages:
  - lintdocs

before_script:
  - cat /etc/*release
  - spell --version
  - mdl --version

lintdocs:
  stage: lintdocs
  script:
    - spell -d .spellrc README.md
    - mdl README.md
    - rst-lint README.rst
  only:
    changes:
      - README.md
...
```

## Features

### Distribution

The image is based off of the slim version of the official Dockerhub image of the distro indicated in the `.gitlab-ci.yml` file.

### Language

The image contains the version of `ruby` indicated in the `.gitlab-ci.yml` file.

### Packages

This image contains the following additional utilities on top of the base image's utils:

- The markdownlint ruby gem (mdl)
- The GNU Spell dpkg (spell)
- The Restructured text linter dpkg (rst-lint)
- The more advanced and configurable spellchecking (pyspelling)

The versions of the included packages were the latest available in their respective repositories at the time the image was built.

#### Entrypoint

The image has a "/bin/bash" entrypoint.
